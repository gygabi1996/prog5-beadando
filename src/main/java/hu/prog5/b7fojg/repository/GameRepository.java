package hu.prog5.b7fojg.repository;

import hu.prog5.b7fojg.entity.Game;
import hu.prog5.b7fojg.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;


@Repository
public interface GameRepository extends JpaRepository<Game,Integer> {
    Game save(Game game);
    List<Game> findAll();

    Game findById(int id);

    List<Game> findAllByPrice(int price);

    Game findByGameName(String name);

    @Query("select g from User u inner join Game g where u.userName = ?1")
    List<Game> findAllByUserName(String userName);


}
