package hu.prog5.b7fojg.repository;

import hu.prog5.b7fojg.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
    User findByUserName(String userName);
    User findById(int Id);
    List<User> findAll();
}