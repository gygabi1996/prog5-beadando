package hu.prog5.b7fojg.service;

import hu.prog5.b7fojg.entity.Game;
import hu.prog5.b7fojg.entity.User;
import hu.prog5.b7fojg.repository.GameRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
public class GameService {

    private GameRepository gameRepository;

    public Game save(Game game){return gameRepository.save(game); }

    public List<Game> findAll(){return gameRepository.findAll();}

    public List<Game> findAllByPrice(int price) {return gameRepository.findAllByPrice(price);}

    public Game findById(int id) {return gameRepository.findById(id);}

    public Game findByGameName(String name) {return gameRepository.findByGameName(name);}

}
