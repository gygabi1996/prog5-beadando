package hu.prog5.b7fojg.controller;

import hu.prog5.b7fojg.entity.Game;
import hu.prog5.b7fojg.entity.User;
import hu.prog5.b7fojg.service.GameService;
import hu.prog5.b7fojg.service.UserService;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@NoArgsConstructor
public class GameController {

    @Autowired
    private GameService gameService;
    @Autowired
    private UserService userService;

    @GetMapping(value="/games")
    public String games(Model model){
        List<Game> games = gameService.findAll();
        model.addAttribute("games", games);
        return "games";
    }

    @GetMapping(value="/buygame")
    public String gameBuy(Model model, Authentication authentication,
                          @RequestParam("id") int id, @RequestParam("name") String name,
                          @RequestParam("description") String description, @RequestParam("price")int price
                          ){
        String username = authentication.getName();
        User user = userService.findUserByUserName(username);

        user.getGames().add(new Game(id,name,description,price));
        userService.saveUser(user);


        Set<Game> games = user.getGames();
        model.addAttribute("games", games);
        return "mygames";
    }

    @GetMapping(value="/mygames")
    public String myGames(Model model, Authentication authentication){
        String username = authentication.getName();
        User user = userService.findUserByUserName(username);

        Set<Game> games = user.getGames();

        model.addAttribute("games", games);
        return "mygames";
    }

    @GetMapping(value="/admin/add_game")
    public ModelAndView newGame(){
        ModelAndView modelAndView = new ModelAndView();
        Game game = new Game();
        modelAndView.addObject("game", game);
        modelAndView.setViewName("admin/add_game");
        return modelAndView;
    }

    @PostMapping(value="/admin/add_game")
    public ModelAndView createNewGame(@Valid Game game) {
        ModelAndView modelAndView = new ModelAndView();
        gameService.save(game);
        modelAndView.addObject("successMessage", true);
        modelAndView.addObject("game", new Game());
        modelAndView.setViewName("admin/add_game");

        return modelAndView;
    }

}
