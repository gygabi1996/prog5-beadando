package hu.prog5.b7fojg.controller.rest;
import hu.prog5.b7fojg.entity.Game;
import hu.prog5.b7fojg.entity.User;
import hu.prog5.b7fojg.service.GameService;
import hu.prog5.b7fojg.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
@RestController
@RequiredArgsConstructor
public class UserRestController {

    @Autowired
    private final UserService userService;

    @GetMapping("/api/userbyid")
    public User findUserById(int id) {return userService.findUserById(id);}

    @GetMapping("/api/userbyusername")
    public User findUserByUserName(String userName) {
        return userService.findUserByUserName(userName);
    }

    @GetMapping("/api/userbyemail")
    public User findUserByEmail(String email) {
        return userService.findUserByEmail(email);
    }

    @GetMapping("/api/usersall")
    public List<User> findAll() {return userService.findAll();}
}
