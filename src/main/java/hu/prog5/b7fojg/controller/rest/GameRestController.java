package hu.prog5.b7fojg.controller.rest;
import hu.prog5.b7fojg.entity.Game;
import hu.prog5.b7fojg.service.GameService;
import hu.prog5.b7fojg.service.UserService;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
@RestController
@RequiredArgsConstructor
public class GameRestController {

    @Autowired
    private final GameService gameService;

    @GetMapping("/api/gamebyid")
    public Game findById(int id) {
        return this.gameService.findById(id);
    }

    @GetMapping("/api/gamebyname")
    public Game findByGameName(String name) {return gameService.findByGameName(name);}

    @GetMapping("/api/gamesbyprice")
    public List<Game> findAllByPrice(int price) {return gameService.findAllByPrice(price);}

    @GetMapping("/api/gamesall")
    public List<Game> findAll(){return gameService.findAll();}
}
