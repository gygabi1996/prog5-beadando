package hu.prog5.b7fojg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class B7fojgApplication {

	public static void main(String[] args) {
		SpringApplication.run(B7fojgApplication.class, args);
	}

}
